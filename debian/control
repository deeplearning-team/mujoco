Source: mujoco
Section: science
Homepage: https://mujoco.org/
Priority: optional
Standards-Version: 4.6.0.1
Vcs-Git: https://salsa.debian.org/deeplearning-team/mujoco.git
Vcs-Browser: https://salsa.debian.org/deeplearning-team/mujoco
Maintainer: Debian Deep Learning Team <debian-ai@lists.debian.org>
Uploaders: Mo Zhou <lumin@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               dh-exec,
               libabsl-dev <!nocheck>,
               libbenchmark-dev <!nocheck>,
               libccd-dev,
               libeigen3-dev,
               libglfw3-dev,
               libgmock-dev <!nocheck>,
               libgtest-dev <!nocheck>,
               liblodepng-dev,
               libqhull-dev,
               libtinyobjloader-dev,
               libtinyxml2-dev,
               ninja-build

Package: libmujoco2.2.2
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Multi-Joint dynamics with Contact. A general purpose physics simulator.
 MuJoCo stands for Multi-Joint dynamics with Contact. It is a general purpose
 physics engine that aims to facilitate research and development in robotics,
 biomechanics, graphics and animation, machine learning, and other areas which
 demand fast and accurate simulation of articulated structures interacting with
 their environment.
 .
 MuJoCo has a C API and is intended for researchers and developers. The runtime
 simulation module is tuned to maximize performance and operates on low-level
 data structures that are preallocated by the built-in XML compiler. The library
 includes interactive visualization with a native GUI, rendered in OpenGL.
 MuJoCo further exposes a large number of utility functions for computing
 physics-related quantities. We also provide Python bindings and a plug-in for
 the Unity game engine.
 .
 This package contains the shared object.

Package: libmujoco-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libmujoco2.2.2 (= ${binary:Version}), ${misc:Depends}
Description: Multi-Joint dynamics with Contact. A general purpose physics simulator.
 MuJoCo stands for Multi-Joint dynamics with Contact. It is a general purpose
 physics engine that aims to facilitate research and development in robotics,
 biomechanics, graphics and animation, machine learning, and other areas which
 demand fast and accurate simulation of articulated structures interacting with
 their environment.
 .
 MuJoCo has a C API and is intended for researchers and developers. The runtime
 simulation module is tuned to maximize performance and operates on low-level
 data structures that are preallocated by the built-in XML compiler. The library
 includes interactive visualization with a native GUI, rendered in OpenGL.
 MuJoCo further exposes a large number of utility functions for computing
 physics-related quantities. We also provide Python bindings and a plug-in for
 the Unity game engine.
 .
 This package contains the development files.

Package: libmujoco-samples
Section: libdevel
Architecture: any
Multi-Arch: foreign
Depends: libmujoco2.2.2 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Multi-Joint dynamics with Contact. A general purpose physics simulator.
 MuJoCo stands for Multi-Joint dynamics with Contact. It is a general purpose
 physics engine that aims to facilitate research and development in robotics,
 biomechanics, graphics and animation, machine learning, and other areas which
 demand fast and accurate simulation of articulated structures interacting with
 their environment.
 .
 MuJoCo has a C API and is intended for researchers and developers. The runtime
 simulation module is tuned to maximize performance and operates on low-level
 data structures that are preallocated by the built-in XML compiler. The library
 includes interactive visualization with a native GUI, rendered in OpenGL.
 MuJoCo further exposes a large number of utility functions for computing
 physics-related quantities. We also provide Python bindings and a plug-in for
 the Unity game engine.
 .
 This package contains the samples binary executables and sample models.
